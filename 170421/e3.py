"""Exercise 3."""
import re
import sys


def is_empty_with_regex(string):
    """check if a string is empty with a regrex."""
    empty_pattern = r"^$"
    if re.match(empty_pattern, string):
        return True
    else:
        return False

def is_empty_without_regex(string):
    """check if a string is empty without a regrex."""
    if string == '':
        return True
    else:
        return False

def is_empty(string, func):
    """Check if a string is empty using a given function."""
    print('Function name: {}'.format(func.__name__))
    print('String: `{}`'.format(string))
    if func(string):
        print(' -> EMPTY'.format(func.__name__, string))
    else:
        print(' -> NOT EMPTY'.format(func.__name__, string))
    
def main():
    strings = ['', 'kiyomaru']
    functions = [is_empty_with_regex, is_empty_without_regex]

    for string in strings:
        for function in functions:
            is_empty(string, function)

    return 0


if __name__ == '__main__':
    sys.exit(main())
