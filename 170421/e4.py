"""Exercise 4."""
import re
import sys


VOWELS = ['a', 'i', 'u', 'e', 'o']

def double_vowels_with_regex(string):
    """Double all vowels in string with a regrex."""
    def double(matchobj):
        return matchobj.group(0) * 2
    string = re.sub(r'a|i|u|e|o', double, string)
    return string

def double_vowels_without_regex(string):
    """Double all vowels in string without a regrex."""
    for vowel in VOWELS:
        string = string.replace(vowel, vowel * 2)
    return string

def double_vowels(string, func):
    """Double all vowels in string."""
    print('Function name: {}'.format(func.__name__))
    print('String: `{}`'.format(string))
    print(' -> `{}`'.format(func(string)))

def main():
    string = 'kiyomaru'
    functions = [double_vowels_with_regex, double_vowels_without_regex]

    for function in functions:
        double_vowels(string, function)

    return 0


if __name__ == '__main__':
    sys.exit(main())
