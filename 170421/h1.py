"""Implementation of Ngram language model."""
from collections import defaultdict
from corenlp import StanfordCoreNLP
import gzip
import json
import re
import sys


CORPUS_PATH = '/share/text/WWW.en/txt.en/tsubame00/doc0000000000.txt.gz'
CORE_NLP_PATH = '/home/kiyomaru/.lib/stanford-corenlp-full-2013-06-20'
CORE_NLP_PROP_PATH = '/home/kiyomaru/.lib/user.properties'


class NgramLM(object):
    """Ngram language model, for use on a given corpus."""

    def __init__(self, n, path_to_corpus, path_to_corenlp, path_to_corenlp_prop):
        """Construct a `NgramLM.`
        
        Args:
            n: Number of window size.
            path_to_corpus: Path to corpus.
            path_to_corenlp: Path to CoreNLP.
            path_to_corenlp_prop: Path to property file.
        """

        self._n = n
        self._path_to_corpus = path_to_corpus

        self._ngram = defaultdict(lambda: defaultdict(int))
        self._ngram_denom = defaultdict(int)  # denominators for the n-gram

        self.parser = StanfordCoreNLP(
            corenlp_path=path_to_corenlp,
            properties=path_to_corenlp_prop
        )
        
        self.build()
        
    def build(self):
        """Build Ngram model."""

        with gzip.open(self._path_to_corpus) as f:
            for line in f:
                line = line.decode('utf-8').strip()
                if self._filter_out(line):
                    continue
                else:
                    self._count_token(line)
                print(".", end="", flush=True)  # Confirm progress

    def estimate(self, sequence):
        """Estimate probability for given sequence.

        Args:
            sequence: Sequence of characters.
        """

        ngram = self._get_ngram(sequence)

        prob = 1.0
        for element in ngram:
            key1 = '&'.join(element[:-1])
            key2 = element[-1]
            prob *= (self._ngram[key1][key2] / self._ngram_denom[key1])

        print(prob)
    
    def _count_token(self, sequence):
        """Count tokens in given sequence.

        Args:
            sequence: Sequence of characters.
        """

        ngram = self._get_ngram(sequence)

        if ngram:
            for element in ngram:
                try:
                    key1 = '&'.join(element[:-1])
                    key2 = element[-1]
                except:
                    from IPython import embed
                    embed()
                self._ngram[key1][key2] += 1
                self._ngram_denom[key1] += 1
        else:
            pass    

    def _get_ngram(self, sequence):
        """Extract Ngram of given sequence.

        Args:
            sequence: Sequence of characters.
        """

        tokens = self._get_tokens(sequence)

        if tokens:
            ngram = list(zip(*[tokens[i:] for i in range(self._n)]))
        else:
            ngram = []

        return ngram

    def _get_tokens(self, sequence):
        """Extract tokens of given sequence.

        Args:
            sequence: Sequence of characters.
        """

        try:
            result = json.loads(self.parser.parse(sequence))
            result = result['sentences'][0]
            tokens = [word[0] for word in result['words']]
            tokens = list(set(tokens) - set([None]))  # remove empty token
            if tokens:
                tokens = ['<FILL_TOKEN>'] * (self._n - 1) + tokens
            else:
                tokens = []
        except:
            tokens = []

        return tokens

    def _filter_out(self, sequence):
        """Filter out non-English sequence.

        Args:
            sequence: Sequence of characters.
        """

        pattern0 = r"^$"
        pattern1 = r"^<PAGE URL="
        pattern2 = r"</PAGE>"

        if re.match(pattern0, sequence):
            return True
        if re.match(pattern1, sequence):
            return True
        if re.match(pattern2, sequence):
            return True
        return False


def main():
    """Entry point."""

    bigram = NgramLM(
        n=2,
        path_to_corpus=CORPUS_PATH,
        path_to_corenlp=CORE_NLP_PATH,
        path_to_corenlp_prop=CORE_NLP_PROP_PATH
    )
    
    sequence = "The man is in the house."
    bigram.estimate(sequence)

    return 0


if __name__ == '__main__':
    sys.exit(main())
