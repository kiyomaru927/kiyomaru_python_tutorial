"""Exercise 2."""
import argparse
import sys


"""
Usage
python e2.py [input_file_path] [output_file_path]
"""

parser = argparse.ArgumentParser()
parser.add_argument('f', type=str,
                    help='Path to input file.')
parser.add_argument('o', type=str,
                    help='Path to output file.')
args = parser.parse_args()

def cp(path_to_input, path_to_output):
    with open(path_to_output, 'w') as output_file:
        with open(path_to_input, 'r') as input_file:
            for line in input_file:
                output_file.write(line)
    return 0

def main(args):
    return cp(args.input_file, args.output_file)


if __name__ == '__main__':
    sys.exit(main(args))
