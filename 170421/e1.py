"""Exercise 1."""
import math
import sys


def circle_area(radius):
    area = radius * radius * math.pi
    return area

def main():
    area = circle_area(3.5)
    print("Circle area: {0:.2f}".format(area))
    return 0


if __name__ == '__main__':
    sys.exit(main())
