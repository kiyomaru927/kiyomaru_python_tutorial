"""homework 5."""

def main():
    for i in range(1, 9 + 1):
        for j in range(1, 9 + 1):
            print('{0:2d}'.format(i * j), end=' ')
        print('')


if __name__ == '__main__':
    main()

