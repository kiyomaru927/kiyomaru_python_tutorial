"""homework 3."""

def main():
    sum = 0
    for i in range(2, 30 + 1, 2):
        sum += i
    print('sum is {}'.format(sum))


if __name__ == '__main__':
    main()


