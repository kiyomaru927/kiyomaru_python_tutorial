"""homework 7."""

def main():
    for i in range(1, 1000 + 1):
        num_of_devisor = 0
        for j in range(1, i + 1):
            if i % j == 0:
                num_of_devisor += 1
        if num_of_devisor == 2:
            print(i)


if __name__ == '__main__':
    main()
