
if __name__ == '__main__':
    # (a)
    print("[not (1 == 1)] is ")
    if not (1 == 1):
        print('True')
    else:
        print('False')
    # (b)
    print("[2 == 1 + 1] is ")
    if 2 == 1 + 1:
        print('True')
    else:
        print('False')
    # (c)
    print("[0.9 - 0.6 ==0.3] is ")
    if 0.9 - 0.6 ==0.3:
        print('True')
    else:
        print('False')
    # (d)
    print("[True or True or False] is ")
    if True or True or False:
        print('True')
    else:
        print('False')

