""""Exercise 8."""

def main():
    string1 = 'パトカー'
    string2 = 'タクシー'
    joined_string = ''.join([''.join(j) for i in zip(string1, string2) for j in i])
    print(joined_string)


if __name__ == '__main__':
    main()
