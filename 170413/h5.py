""""Homework 5."""


def calculate_gcd(a, b):
    if b > a:
        a, b = b, a
    while b:
        a, b = b, a % b
    return a


def main():
    a = 42
    b = 56
    gcd = calculate_gcd(a, b)
    print(gcd)


if __name__ == '__main__':
    main()
