""""Homework 7."""
import copy
import random


def bubble_sort(array):
    array_size = len(array)
    for i in range(array_size):
        for j in range(array_size-1-i):
            if array[j] > array[j+1]:
                array[j], array[j+1] = array[j+1], array[j]

    return array

def quick_sort(array):
    array_size = len(array)
    if array_size > 1:
        pivot = array[0]
        rest_array = array[1:]
        smaller_elements = [i for i in rest_array if i < pivot]
        larger_elements = [i for i in rest_array if i >= pivot]
        smaller_elements = quick_sort(smaller_elements)
        larger_elements = quick_sort(larger_elements)
        array = smaller_elements + [pivot] + larger_elements
        
    return array

def merge_sort(array):
    array_size = len(array)
    if array_size > 1:
        middle = int(array_size / 2)
        l_array = array[0: middle]
        r_array = array[middle:]

        l_array = merge_sort(l_array)
        r_array = merge_sort(r_array)

        array = []
        while l_array and r_array:
            if l_array[0] < r_array[0]:
                array.append(l_array.pop(0))
            else:
                array.append(r_array.pop(0))
        if l_array:
            array += l_array
        elif r_array:
            array += r_array

    return array

def apply_sort(sort_func, array):
    algorithm = sort_func.__name__
    print('Sorting algorithm: {}'.format(algorithm))
    print('Original array: {}'.format(array))
    print('Sorted array  : {}'.format(sort_func(array)))
    print('--------------------------------')

def main():
    array = list(range(10))
    random.shuffle(array)

    array1 = copy.deepcopy(array)
    array2 = copy.deepcopy(array)
    array3 = copy.deepcopy(array)

    apply_sort(bubble_sort, array1)
    apply_sort(quick_sort, array2)
    apply_sort(merge_sort, array3)


if __name__ == '__main__':
    main()
