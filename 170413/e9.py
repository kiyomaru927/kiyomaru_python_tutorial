""""Exercise 9."""

def main():
    string = "Hi He Lied Because Boron Could Not Oxidize Fluorine. New Nations Might Also Sign Peace Security Clause. Arthur King Can."
    filter_num = [1, 5, 6, 7, 8, 9, 15, 16, 19]
    words = string.split(' ')
    beginnings = ' '.join([word[0] if (i + 1) in filter_num else word[:2] for i, word in enumerate(words)])
    print(beginnings)


if __name__ == '__main__':
    main()
