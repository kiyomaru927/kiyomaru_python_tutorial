""""Homework 6."""
import math


"""
(this script)
$ time python h6.py
-> python h6.py  0.07s user 0.02s system 83% cpu 0.105 total
---
(previous script)
$ time python h7.py
-> python h7.py  7.63s user 0.01s system 99% cpu 7.664 total
"""
def get_primes(n):
    primes = []
    not_primes = []
    search_list = range(2, n + 1)
    sqrt_of_n = math.sqrt(n)
    for i in search_list:
        if i > sqrt_of_n:
            break
        elif i in not_primes:
            continue
        else:
            primes.append(i)
            not_primes += [j for j in search_list if j % i == 0]
    primes = sorted(primes + list(set(search_list) - set(not_primes)))
    return primes

def main():
    n = 10000
    primes = get_primes(n)
    print(primes)


if __name__ == '__main__':
    main()
