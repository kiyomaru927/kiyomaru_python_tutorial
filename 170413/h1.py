""""Homework 1."""

def get_maximum(a, b, c):
    return max([a, b, c])
    

def main():
    a = 1994
    b = 9
    c = 27
    maximum_number = get_maximum(a, b, c)
    print(maximum_number)


if __name__ == '__main__':
    main()
