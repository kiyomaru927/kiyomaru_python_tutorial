""""Homework 4-1."""
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('n', type=int, default=-1)
args = parser.parse_args()


"""
T(n) = T(n-1) + T(n-2) + O(1)
-> O(a^n)
"""
def fibonacci(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibonacci(n-1) + fibonacci(n-2)

def main(args):
    n = args.n
    if n < 1:
        print('{} is an invalid number.'.format(n))
    else:
        n_th_number = fibonacci(n)
        print('n-th number is {}.'.format(n_th_number))


if __name__ == '__main__':
    main(args)
