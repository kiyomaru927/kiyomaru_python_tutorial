""""Exercise 2."""

def main():
    even_numbers = [i for i in range(1, 31) if i % 2 == 0]
    print(even_numbers)
    even_numbers = [0 if i % 3 == 0 else i for i in even_numbers]
    print(even_numbers)


if __name__ == '__main__':
    main()
