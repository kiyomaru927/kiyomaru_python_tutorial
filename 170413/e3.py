""""Exercise 3."""
import argparse


parser = argparse.ArgumentParser()
parser.add_argument('n', type=int)
args = parser.parse_args()

DAYS = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']


def main(args):
    index = int(args.n) - 1
    day = DAYS[index]
    print(day)


if __name__ == '__main__':
    main(args)
