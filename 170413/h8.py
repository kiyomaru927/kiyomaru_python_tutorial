""""Homework 8."""


class NQueen(object):
    """NQueen, the problem of placing N chess queens
       so that no two queens threaten each other."""
    
    def __init__(self, n=8):
        """Construct a `NQueen`.
        
        Args:
            n: Number of queens.
        """
        
        self._n = n  # number of queens
        self._count = 0  # number of valid patterns
        self._board = []  # chess board which has x-coordinates of queens

    def run(self):
        """Run the algorithm to solve NQueen."""
        
        print('{}-QUEEN PROBLEM'.format(self._n))
        self._n_queen(0)
        print('  -> {} solutions are found.'.format(self._count))

    def _n_queen(self, y):
        """Place y-th queen and check it.
        
        Args:
            y: Numbeer of queens which are already placed.
        """
        
        if self._n == y:
            if self._check_board():
                self._count += 1
        else:
            for x in range(self._n):
                if self._vertical_conflict(x):
                    continue
                else:
                    self._board.append(x)  # place a queen at (x, y)
                    self._n_queen(y+1)
                    self._board.pop()  # remove a queen at (x, y)

    def _check_board(self):
        """Check whether current _board is valid or not."""
        
        for y in range(self._n):
            if self._diagonal_conflict(self._board[y], y):
                return False
        return True

    def _vertical_conflict(self, x):
        """Check vertical conflict for given x.

        Args:
            x: x coordinate of a queen.
        """

        if x in self._board:
            return True
        return False

    def _diagonal_conflict(self, x, y):
        """Check diagonal conflict for given x.

        Args:
            x: x coordinate of a queen.
            y: y coordinate of a queen.
        """

        for y_i in range(y):
            x_i = self._board[y_i]
            if (x_i - y_i == x - y) or (x_i + y_i == x + y):
                return True
        return False


def main():
    NQueen().run()


if __name__ == '__main__':
    main()
