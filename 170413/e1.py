""""Exercise 1."""

def main():
    source_list = [1, 2, 3]
    dest_list = [i for i in source_list]
    print(dest_list)


if __name__ == '__main__':
    main()
