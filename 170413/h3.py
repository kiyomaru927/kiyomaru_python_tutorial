""""Homework 3."""

def fill_zero_except_first_element(array):
    new_array = [element if i == 0 else 0 for i, element in enumerate(array)]
    return new_array


def main():
    array = [1994, 9, 27]
    array = fill_zero_except_first_element(array)
    print(array)


if __name__ == '__main__':
    main()
