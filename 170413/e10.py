""""Exercise 10."""
from collections import defaultdict


def main():
    words = ['eggs', 'spam', 'spam', 'bacon']
    frequency = defaultdict(int)
    for word in words:
        frequency[word] += 1
    print(frequency)
    

if __name__ == '__main__':
    main()
