""""Homework 2."""

def get_maximum_with_index(numbers):
    maximum_number = max(numbers)
    index = numbers.index(maximum_number)
    return index, maximum_number

def main():
    numbers = [1994, 9, 27]
    index, maximum_number = get_maximum_with_index(numbers)
    print('{}: {}'.format(index, maximum_number))


if __name__ == '__main__':
    main()
