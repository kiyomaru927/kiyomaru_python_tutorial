""""Homework 4-2."""
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('n', type=int, default=-1)
args = parser.parse_args()


"""
T(n) = O(n)
-> O(n)
"""
def fibonacci(n):
    a = 0
    b = 1
    for i in range(n):
        a, b = b, a + b
    return a

def main(args):
    n = args.n
    if n < 1:
        print('{} is an invalid number.'.format(n))
    else:
        n_th_number = fibonacci(n)
        print('n-th number is {}.'.format(n_th_number))


if __name__ == '__main__':
    main(args)
