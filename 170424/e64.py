# -*- coding: utf-8 -*-

"""Exercise 64."""
from collections import Counter
import sys
from pyknp import Jumanpp


def sort_genkei():
    jumanpp = Jumanpp()
    dictionary = Counter()
    data = ''
    for line in iter(sys.stdin.readline, ''):
        data += line
        if line.strip() == 'EOS':
            result = jumanpp.result(data)
            for mrph in result.mrph_list():
                dictionary[mrph.genkei] += 1
            data = ''
    print('\n'.join(['{0} {1}'.format(k, v) for k, v in dictionary.most_common()]))

def main():
    sort_genkei()


if __name__ == '__main__':
    sys.exit(main())
