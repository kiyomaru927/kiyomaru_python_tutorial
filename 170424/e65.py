# -*- coding: utf-8 -*-

"""Exercise 65."""
import sys
from pyknp import Jumanpp


def get_ratio_of_predicate():
    jumanpp = Jumanpp()

    denominator = 0
    predicate = 0
    predicate_list = ['動詞', '形容詞']
    
    data = ''
    for line in iter(sys.stdin.readline, ''):
        data += line
        if line.strip() == 'EOS':
            result = jumanpp.result(data)
            for mrph in result.mrph_list():
                denominator += 1
                if mrph.hinsi in predicate_list:
                    predicate += 1
            data = ''
    print('Ratio of Predicate: {0:.2f}'.format(predicate / denominator))

def main():
    get_ratio_of_predicate()


if __name__ == '__main__':
    sys.exit(main())
