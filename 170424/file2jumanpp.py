#coding: utf-8 
from pyknp import Jumanpp
import sys

jumanpp = Jumanpp()

data = ""
for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む
    data += line
    if line.strip() == "EOS": # 1文が終わったら解析
        result = jumanpp.result(data)
        print(",".join(mrph.midasi for mrph in result.mrph_list()))
        data = ""
