# -*- coding: utf-8 -*-

"""Exercise 67."""
import sys
from pyknp import Jumanpp


def get_a_of_b():
    jumanpp = Jumanpp()
    
    data = ''.join(sys.stdin.readlines())
    result = jumanpp.result(data)

    for i, j, k in zip(result.mrph_list(), result.mrph_list()[1:], result.mrph_list()[2:]):
        if i.hinsi == '名詞' and j.genkei == 'の' and k.hinsi == '名詞':
            print(i.midasi, j.midasi, k.midasi)

def main():
    get_a_of_b()


if __name__ == '__main__':
    sys.exit(main())
