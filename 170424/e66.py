# -*- coding: utf-8 -*-

"""Exercise 66."""
import sys
from pyknp import Jumanpp


def print_pattern():
    jumanpp = Jumanpp()
    
    data = ''.join(sys.stdin.readlines())
    result = jumanpp.result(data)

    for i, j in zip(result.mrph_list(), result.mrph_list()[1:]):
        if i.bunrui == 'サ変名詞' and j.genkei in ['する', 'できる']:
            print(i.midasi, j.midasi)

def main():
    print_pattern()


if __name__ == '__main__':
    sys.exit(main())
