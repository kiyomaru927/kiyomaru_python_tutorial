#coding: utf-8
from pyknp import KNP 

# インスタンスを作成
knp = KNP(jumanpp=True)

# 文を解析し、解析結果を Python の内部構造に変換して result に格納
result = knp.parse("望遠鏡で泳いでいる少女を見た。")

# 解析結果へのアクセス
# 文節、基本句、形態素の3階層からなり、resultから各階層へアクセスできる。
# また各階層からは下位の階層へアクセスできる。

# 1.各文節へのアクセス: bnst_list()
for bnst in result.bnst_list():
    print("文節ID:", bnst.bnst_id)
    print("見出し:", "".join(mrph.midasi for mrph in bnst.mrph_list()))
    print("係り受けタイプ:", bnst.dpndtype)
    print("親文節ID:", bnst.parent_id)
    print("正規化代表表記:", bnst.repname)
    print("素性:", bnst.fstring)
    print("--------------------")
    print("\n\n")
    
# 2.各基本句へのアクセス: tag_list()
for tag in result.tag_list():
    print("基本句ID:", tag.tag_id)
    print("見出し:", "".join(mrph.midasi for mrph in tag.mrph_list()))
    print("係り受けタイプ:", tag.dpndtype)
    print("親基本句ID:", tag.parent_id)
    print("正規化代表表記:", tag.repname)
    print("素性:", tag.fstring)
    print("--------------------")
    print("\n\n")
        
# 3.各形態素へのアクセス: mrph_list()
for mrph in result.mrph_list():
    print("見出し:", mrph.midasi)
    print("読み:", mrph.yomi)
    print("原形:", mrph.genkei)
    print("品詞:", mrph.hinsi)
    print("品詞細分類:", mrph.bunrui)
    print("活用型:", mrph.katuyou1)
    print("活用形:", mrph.katuyou2)
    print("意味情報:", mrph.imis)
    print("代表表記:", mrph.repname)
    print("--------------------")
