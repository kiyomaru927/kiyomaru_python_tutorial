# -*- coding: utf-8 -*-

"""Exercise 62."""
import sys
from pyknp import Jumanpp


def meisi_stdinput():
    jumanpp = Jumanpp()
    data = ''
    for line in iter(sys.stdin.readline, ''):
        data += line
        if line.strip() == 'EOS':
            result = jumanpp.result(data)
            print(','.join(mrph.midasi for mrph in result.mrph_list() if mrph.hinsi == '名詞'))
            data = ''

def main():
    meisi_stdinput()


if __name__ == '__main__':
    sys.exit(main())
