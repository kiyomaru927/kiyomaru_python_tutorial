# -*- coding: utf-8 -*-

"""Exercise 63."""
import sys
from pyknp import Jumanpp


def genkei():
    jumanpp = Jumanpp()
    data = ''
    for line in iter(sys.stdin.readline, ''):
        data += line
        if line.strip() == 'EOS':
            result = jumanpp.result(data)
            print(','.join(mrph.genkei for mrph in result.mrph_list() if mrph.hinsi == '動詞'))
            data = ''

def main():
    genkei()

    
if __name__ == '__main__':
    sys.exit(main())
