#coding: utf-8
from pyknp import Jumanpp

# インスタンスを作成
jumanpp = Jumanpp()

# 文を解析し、解析結果を Python の内部構造に変換して result に格納
result = jumanpp.analysis("子どもはリンゴが好きだった")

# 解析結果へのアクセス
for mrph in result.mrph_list():
    print("見出し:", mrph.midasi)
    print("読み:", mrph.yomi)
    print("原形:", mrph.genkei)
    print("品詞:", mrph.hinsi)
    print("品詞細分類:", mrph.bunrui)
    print("活用型:", mrph.katuyou1)
    print("活用形:", mrph.katuyou2)
    print("意味情報:", mrph.imis)
    print("代表表記:", mrph.repname)
    print("--------------------")

