"""Exercise 68."""
import sys
from pyknp import KNP


def parse():
    knp = KNP(jumanpp=True)
    sequence = sys.stdin.readline()
    sequence = sequence.strip()
    result = knp.parse(sequence)
    print(' '.join("".join(mrph.midasi for mrph in bnst.mrph_list()) for bnst in result.bnst_list()))

def main():
    parse()

if __name__ == '__main__':
    sys.exit(main())
