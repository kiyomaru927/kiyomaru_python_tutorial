"""Exercise 61."""
import sys
from pyknp import Jumanpp


def tokenize_stdinput():
    jumanpp = Jumanpp()
    sequence = sys.stdin.readline()
    sequence = sequence.strip()
    result = jumanpp.analysis(sequence)
    print(' '.join([mrph.midasi for mrph in result.mrph_list()]))

def main():
    tokenize_stdinput()

if __name__ == '__main__':
    sys.exit(main())
