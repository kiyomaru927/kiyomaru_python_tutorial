"""Exercise 70."""
import sys
from pyknp import KNP


def two_nouns():
    knp = KNP(jumanpp=True)
    data = ''
    for line in iter(sys.stdin.readline, ''):
        data += line
        if line.strip() == 'EOS':
            result = knp.result(data)
            for bnst in result.bnst_list():
                if sum(mrph.hinsi == '名詞' for mrph in bnst.mrph_list()) >= 2:
                    print(''.join([mrph.midasi for mrph in bnst.mrph_list()]))
            data = ''

def main():
    two_nouns()

if __name__ == '__main__':
    sys.exit(main())
